# Super heroes REST API in Ruby Sinatra

CRUD app for creating, reading, editing and destroying super heroes using Ruby Sinatra REST Api.
If you have ruby already installed go to https://rubygems.org search sinatra then open a command prompt to paste this and 

    gem install sinatra

The set up is simple, to begin define the module in the modules folder.

    super.rb
Make these declarations in the module file:

    class Super <  ActiveRecord::Base
        validates :name, presence: true
        validates :power, presence: true
    end
Before setting up the main file, set up 3 configuration files:

    config.ru
    Gemfile
    Rakefile

Config.ru requires the use of the main application
    app.rb

Gemfile has all the gems which are files necessary to run the app, to install all of them run these codes.

    gem install bundler
    bundle install

Rakefile is a task file.

    require "sinatra/activerecord/rake"

    namespace :db do
        task :load_config do
            require "./app"
        end
    end
You can either set the database in the main app or in a yaml file.

    # app.rb
    require "sinatra/activerecord"

    set :database, {adapter: "sqlite3", database: "bit.sqlite3"}
    # or set :database_file, "path/to/database.yml"

Then create a migration to the database:

    bundle exec rake db:create_migration NAME=create_supers

Define the table in migration the file automatically created in the directory db/migrate:

    class CreateSupers < ActiveRecord::Migration
        def change
            create_table :supers do |t|
                t.string :name
                t.string :power
            end
        end
    end

Run the migration:

    bundle exec rake db:migrate

In the module file super.rb you can do this:

    class User < ActiveRecord::Base
        validates_presence_of :name
    end
Which can come in handy when declaring relations between tables.

To run the app in the main folder/repository simply do in the terminal:

    ruby app.rb

An example of intereacting with the resources of the Api which can be done in Postman or curl:
require 'json

    get '/api/supers' do
        @supers = Super.all
        @supers.json
    end

For more on Sinatra: sinatrarb.com/intro.html
