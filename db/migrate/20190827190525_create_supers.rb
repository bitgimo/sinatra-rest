class CreateSupers < ActiveRecord::Migration[5.2]
  def change
    create_table :supers do |t|
      t.string :name
      t.string :power
    end
  end
end
