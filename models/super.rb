class Super <  ActiveRecord::Base
    validates :name, presence: true
    validates :power, presence: true
end