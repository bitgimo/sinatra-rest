require 'sinatra'
require 'sinatra/activerecord'
require './models/super'
require 'json'

configure :development do
    set :database, {
        adapter: 'postgresql',
        database: ENV['database'],
        username: ENV['user'],
        password: ENV['password']
    }
end


get '/' do
    "hello Sinatra"
end

get '/api/supers' do
    @supers = Super.all
    @supers.to_json
end

post '/api/supers' do
    data = JSON.parse request.body.read  
    @super = Super.create(
        name: data['name'],
        power: data['power']
    )
   @super.save
   @super.to_json
end

get "/api/supers/:id" do
    @super = Super.find(params[:id])
    @super.to_json
end

put "/api/supers/:id" do
    data = JSON.parse request.body.read  
    @super = Super.find(params[:id])
    @super.update( name: data['name'],
        power: data['power']
    )
    @super.save
    @super.to_json

end    

delete "/api/supers/:id" do
    @super = Super.find(params[:id])
    @super.destroy
    @super.to_json
end

